CREATE DATABASE sapataria;
USE sapataria;

CREATE TABLE calcado (
    idCalcado int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    modelo varchar(200),
    marca varchar(40),
    preco double(6, 2),
    tamanho int,
    estoque boolean

); (editado)

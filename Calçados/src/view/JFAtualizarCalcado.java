package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


import model.bean.Calcado;
import model.bean.Calcado;
import model.dao.CalcadoDAO;
import model.dao.CalcadoDAO;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;
import javax.swing.JFormattedTextField;

public class JFAtualizarCalcado extends JFrame {

	private JPanel contentPane;
	private JTextField txtModelo;
	private JTextField txtMarca;
	private static int id;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFAtualizarCalcado frame = new JFAtualizarCalcado(id);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFAtualizarCalcado(int id) {
		setTitle("CalcadosdoConrado- Atualizar Calcado");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 527, 468);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		CalcadoDAO cdao = new CalcadoDAO();
		Calcado c = cdao.read(id);
		
		JLabel lblNewLabel = new JLabel("Atualizar Calcado");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Snap ITC", Font.BOLD | Font.ITALIC, 24));
		lblNewLabel.setBounds(10, 20, 463, 47);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Modelo");
		lblNewLabel_1.setFont(new Font("Baskerville Old Face", Font.BOLD | Font.ITALIC, 22));
		lblNewLabel_1.setBounds(10, 69, 135, 14);
		contentPane.add(lblNewLabel_1);
		
		txtModelo = new JTextField();
		txtModelo.setBounds(10, 93, 420, 23);
		contentPane.add(txtModelo);
		txtModelo.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Marca");
		lblNewLabel_2.setFont(new Font("Bernard MT Condensed", Font.PLAIN, 15));
		lblNewLabel_2.setBounds(10, 115, 163, 34);
		contentPane.add(lblNewLabel_2);
		
		txtMarca = new JTextField();
		txtMarca.setBounds(0, 147, 211, 20);
		contentPane.add(txtMarca);
		txtMarca.setColumns(10);
		
		JLabel lblNewLabel_3 = new JLabel("Pre\u00E7o");
		lblNewLabel_3.setFont(new Font("Baskerville Old Face", Font.ITALIC, 16));
		lblNewLabel_3.setBounds(10, 177, 201, 22);
		contentPane.add(lblNewLabel_3);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setFont(new Font("Engravers MT", Font.ITALIC, 14));
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancelar.setBounds(316, 384, 175, 36);
		contentPane.add(btnCancelar);
		
		JLabel lblNewLabel_5 = new JLabel("Estoque");
		lblNewLabel_5.setFont(new Font("Bauhaus 93", Font.BOLD | Font.ITALIC, 16));
		lblNewLabel_5.setBounds(0, 239, 102, 14);
		contentPane.add(lblNewLabel_5);
		
		JLabel lblNewLabel_6 = new JLabel("Tamanho");
		lblNewLabel_6.setFont(new Font("Bodoni MT Condensed", Font.BOLD | Font.ITALIC, 20));
		lblNewLabel_6.setBounds(10, 293, 89, 14);
		contentPane.add(lblNewLabel_6);
		
		JRadioButton rdbtnTem = new JRadioButton("TEM");
		rdbtnTem.setFont(new Font("Tempus Sans ITC", Font.PLAIN, 16));
		rdbtnTem.setBounds(10, 259, 102, 23);
		contentPane.add(rdbtnTem);
		
		JRadioButton rdbtnN�otem = new JRadioButton("N\u00C3O TEM");
		rdbtnN�otem.setBounds(108, 259, 193, 23);
		contentPane.add(rdbtnN�otem);
		
		ButtonGroup estoque = new ButtonGroup();
        estoque.add(rdbtnTem);
        estoque.add(rdbtnN�otem);
        
		
		JButton btnCadastrar = new JButton("Atualizar");
		btnCadastrar.setFont(new Font("Bodoni MT Black", Font.BOLD, 16));
		btnCadastrar.setBounds(10, 382, 135, 36);
		contentPane.add(btnCadastrar);
		
		JFormattedTextField txtTamanho = new JFormattedTextField();
		txtTamanho.setBounds(10, 327, 150, 23);
		contentPane.add(txtTamanho);

		JFormattedTextField txtPreco = new JFormattedTextField();
		txtPreco.setBounds(10, 209, 234, 23);
		contentPane.add(txtPreco);
		
		
		JButton btnLimpar = new JButton("Limpar");
		btnLimpar.setFont(new Font("Castellar", Font.BOLD, 16));
		btnLimpar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtModelo.setText(null);
				txtMarca.setText(null);
				txtPreco.setText(null);
				txtTamanho.setText(null);
				estoque.clearSelection();
			}
		});
		btnLimpar.setBounds(151, 382, 150, 44);
		contentPane.add(btnLimpar);
		
		JLabel lblNewLabel_4 = new JLabel("ID:");
		lblNewLabel_4.setBounds(344, 150, 46, 14);
		contentPane.add(lblNewLabel_4);
		
		JLabel lblID = new JLabel("0");
		lblID.setBounds(371, 150, 46, 14);
		contentPane.add(lblID);
		
		lblID.setText(String.valueOf(c.getIdCalcado()));
		txtModelo.setText(c.getModelo());
		txtMarca.setText(c.getMarca());
		txtPreco.setValue(c.getPreco());
		txtTamanho.setValue(c.getTamanho());
		if (c.isEstoque() == true) {
			rdbtnTem.setSelected(true);
			rdbtnN�otem.setSelected(false);
		} else {
			rdbtnTem.setSelected(false);
			rdbtnN�otem.setSelected(true);
		}
		
        
		btnCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Calcado c = new Calcado();
				CalcadoDAO dao = new CalcadoDAO();

				c.setIdCalcado(Integer.parseInt(lblID.getText()));
				c.setModelo(txtModelo.getText());
				c.setMarca(txtMarca.getText());
				c.setPreco(Double.parseDouble(txtPreco.getText()));
				c.setTamanho(Integer.parseInt(txtTamanho.getText()));
				if(rdbtnTem.isSelected()) {
				c.setEstoque(true);
				}else {
				c.setEstoque(false);
				}

				dao.update(c);
				dispose();
			}
		});
	}
}

package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import model.bean.Calcado;
import model.dao.CalcadoDAO;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class JFListarCalcados extends JFrame {

	private JPanel contentPane;
	private JTable JTCalcados;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFListarCalcados frame = new JFListarCalcados();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFListarCalcados() {
		addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				readJTable();
			}
		});
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowActivated(WindowEvent e) {
				readJTable();
			}
		});
		setTitle("Listar Cal�ados");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 620, 421);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Listar Cal�ados");
		lblNewLabel.setFont(new Font("Gill Sans Ultra Bold", Font.BOLD, 16));
		lblNewLabel.setBounds(10, 11, 239, 29);
		contentPane.add(lblNewLabel);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 39, 584, 263);
		contentPane.add(scrollPane);
		
		JTCalcados = new JTable();
		JTCalcados.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
			},
			new String[] {
				"ID Cal\u00E7ado", "Modelo", "Marca", "Pre\u00E7o", "Tamanho", "Estoque"
			}
		));
		scrollPane.setViewportView(JTCalcados);
		
		JButton btnCadastrarCalcado = new JButton("Cadastrar Cal�ado");
		btnCadastrarCalcado.setFont(new Font("Stencil", Font.BOLD, 12));
		btnCadastrarCalcado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFCadastrarCalcado cp = new JFCadastrarCalcado();
				cp.setVisible(true);
			}
		});
		btnCadastrarCalcado.setBounds(10, 318, 165, 36);
		contentPane.add(btnCadastrarCalcado);
		
		JButton btnExcluirCalcado = new JButton("Excluir Cal�ado");
		btnExcluirCalcado.setFont(new Font("Viner Hand ITC", Font.PLAIN, 13));
		btnExcluirCalcado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (JTCalcados.getSelectedRow() != -1) {
					int opcao = JOptionPane.showConfirmDialog(null, "Deseja excluiro calcado?", "Exclus�o", JOptionPane.YES_NO_OPTION);
					if (opcao == 0) {
						CalcadoDAO dao = new CalcadoDAO();
						Calcado c = new Calcado();
						c.setIdCalcado((int)JTCalcados.getValueAt(JTCalcados.getSelectedRow(), 0));
						dao.delete(c);
					}
				} else {
					JOptionPane.showMessageDialog(null, "Selecione um cal�ado!");
				}
				
				readJTable();
			}
		});
		btnExcluirCalcado.setBounds(412, 318, 182, 23);
		contentPane.add(btnExcluirCalcado);
		
		JButton btnAlterarCalcado = new JButton("Alterar Cal�ado");
		btnAlterarCalcado.setFont(new Font("Trebuchet MS", Font.PLAIN, 13));
		btnAlterarCalcado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (JTCalcados.getSelectedRow() != -1) {
					JFAtualizarCalcado ap = new JFAtualizarCalcado((int)JTCalcados.getValueAt(JTCalcados.getSelectedRow(), 0));
					ap.setVisible(true);
				} else {
					JOptionPane.showMessageDialog(null, "Selecione o Cal�ado!");
				}
				
				readJTable();
			}
		});
		btnAlterarCalcado.setBounds(174, 318, 228, 23);
		contentPane.add(btnAlterarCalcado);
		
		readJTable();
	}
	
	public void readJTable() {
		DefaultTableModel modelo = (DefaultTableModel) JTCalcados.getModel();
		modelo.setNumRows(0);
		CalcadoDAO cdao = new CalcadoDAO();
		for(Calcado c : cdao.read()) {
			modelo.addRow(new Object[] {
					c.getIdCalcado(),
					c.getModelo(),
					c.getMarca(),
					c.getPreco(),
					c.getTamanho(),
					c.isEstoque(),
			});
		}
	}
	
}

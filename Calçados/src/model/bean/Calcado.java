package model.bean;

public class Calcado {
	private int idCalcado;
    private String modelo;
    private String marca;
    private double preco;
    private int tamanho;
    private boolean estoque;
    
	public int getIdCalcado() {
		return idCalcado;
	}
	public void setIdCalcado(int idCalcado) {
		this.idCalcado = idCalcado;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public int getTamanho() {
		return tamanho;
	}
	public void setTamanho(int tamanho) {
		this.tamanho = tamanho;
	}
	public double getPreco() {
		return preco;
	}
	public void setPreco(double preco) {
		this.preco = preco;
	}
	public boolean isEstoque() {
		return estoque;
	}
	public void setEstoque(boolean estoque) {
		this.estoque = estoque;
	}
   

}

package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import connection.ConnectionFactory;
import model.bean.Calcado;

public class CalcadoDAO {
	public void create(Calcado c) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		
		try {
			stmt = con.prepareStatement("INSERT INTO calcado (modelo, marca, preco, tamanho, estoque) VALUES (?, ?, ?, ?, ?)");
			stmt.setString(1, c.getModelo());
			stmt.setString(2,c.getMarca());
			stmt.setDouble(3, c.getPreco());
			stmt.setInt(4, c.getTamanho());
			stmt.setBoolean(5, c.isEstoque());
			
			stmt.executeUpdate();
			JOptionPane.showMessageDialog(null, "Salvo com sucesso!");
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao salvar" + e);
		} finally {
			ConnectionFactory.closeConnection(con, stmt);
		}
	}
	
	public List<Calcado> read(){
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<Calcado> calcados = new ArrayList<>();
		
		try {
			stmt = con.prepareStatement("SELECT * FROM calcado");
			rs = stmt.executeQuery();
			while(rs.next()) {
				Calcado c = new Calcado();
				c.setIdCalcado(rs.getInt("idCalcado"));
				c.setModelo(rs.getString("modelo"));
				c.setMarca(rs.getString("marca"));
				c.setPreco(rs.getDouble("preco"));
				c.setTamanho(rs.getInt("tamanho"));
				c.setEstoque(rs.getBoolean("estoque"));
				
				calcados.add(c);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao exibir as informa��es do BD " + e);
			e.printStackTrace();
		} finally {
			ConnectionFactory.closeConnection(con, stmt, rs);
		}
		return calcados;
	}
	
	public void delete(Calcado c) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		
		try {
			stmt = con.prepareStatement("DELETE FROM calcado WHERE idCalcado=?");
			stmt.setInt(1, c.getIdCalcado());
			stmt.executeUpdate();
			
			JOptionPane.showMessageDialog(null, "Cal�ado exclu�do com sucesso!");
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao excluir: " +e);
		} finally {
			ConnectionFactory.closeConnection(con, stmt);
		}
	}
	
	public Calcado read(int id) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Calcado c = new Calcado();
		
		try {
			stmt = con.prepareStatement("SELECT * FROM calcado WHERE idCalcado=? LIMIT 1;");
			stmt.setInt(1, id);
			rs = stmt.executeQuery();
			if (rs != null && rs.next()) {
				c.setIdCalcado(rs.getInt("idCalcado"));
				c.setModelo(rs.getString("modelo"));
				c.setMarca(rs.getString("marca"));
				c.setPreco(rs.getDouble("preco"));
				c.setTamanho(rs.getInt("tamanho"));
				c.setEstoque(rs.getBoolean("estoque"));
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionFactory.closeConnection(con, stmt, rs);
		}
		
		return c;
	}
	
	public void update(Calcado c) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		
		try {
			stmt = con.prepareStatement("UPDATE calcado SET modelo=?, marca=?, preco=?, tamanho=?, estoque=? WHERE idCalcado=?;");
			stmt.setString(1, c.getModelo());
			stmt.setString(2,c.getMarca());
			stmt.setDouble(3, c.getPreco());
			stmt.setInt(4, c.getTamanho());
			stmt.setBoolean(5, c.isEstoque());
			stmt.setInt(6, c.getIdCalcado());
			
			stmt.executeUpdate();
			JOptionPane.showMessageDialog(null, "Alterado com sucesso!");
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao alterar" + e);
		} finally {
			ConnectionFactory.closeConnection(con, stmt);
		}
	}
}
